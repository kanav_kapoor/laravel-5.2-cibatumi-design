$(document).ready(function(){
    $(".button-collapse").sideNav();

    $(".games-dropdown-button").dropdown();
    $(".about-dropdown-button").dropdown();

    $('.carousel.carousel-slider').carousel({full_width: true});
    //setTimeout(autoplay, 8500);
    function autoplay() {
        $('.carousel').carousel('next');
        setTimeout(autoplay, 8500);
    }

    $('.parallax').parallax();
});