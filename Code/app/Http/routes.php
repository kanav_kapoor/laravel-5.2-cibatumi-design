<?php

use Illuminate\Http\Request;

Route::get('/', function () {
    return view('index');
})->name('index');

Route::get('games', function () {
    return view('games');
});

Route::get('about', function () {
    return view('about');
});

Route::post('message', function(Request $request){
    $data['name'] = $request->msg_name;
    $data['email'] = $request->msg_email;
    $data['text'] = $request->msg_text;
    
    try {

        $data['heading'] = 'A new message has been recieved';
        Mail::send('emails.message', $data,  function ($message) use ($request) {
            $message->to('support@cibatumi.com');
            $message->from('no-reply@cibatumi.com', 'Cibatumi');
            $message->subject('Message Recieved - Casino International Batumi');
        });
        
        $data['heading'] = 'We\'ve recieved your message and we\'ll contact you shortly.';
        Mail::send('emails.message', $data,  function ($message) use ($request) {
            $message->to($request->msg_email);
            $message->from('no-reply@cibatumi.com', 'Cibatumi');
            $message->subject('Message Recieved - Casino International Batumi');
        });
    } catch (Exception $e) {
        return ['status' => 'fail'];
    }

    return ['status' => 'success'];

})->name('message');