<?php

    return [
        'casino'        => 'Casino',
        'about_us'      => 'About Us',
        'info'          => 'Info',
        'contact'       => 'contact',
        'games'         => 'Games',
        'table_games'   => 'Table Games',
        'online_casino' => 'Online Casino',
    ];