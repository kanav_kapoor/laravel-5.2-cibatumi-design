<?php

return [
    'block' => [
       [
            'title' => 'Texas Poker',
            'sub_title' => 'The game is played with a deck of 52 cards.',
            'list' => [
                'To start the game, a player must place a bet on the ante.',
                'The dealer deals two cards face down to each player and himself, also opening the additional three cards (the flop) face up on the board, which are community cards.',
                'At this stage of the game the player has two options, to fold or to play. In case if he decides to fold, he will loose the ante, and his cards will be removed. If he decides to play, he has to put the bet, double of the ante.',
                'After the player has made a decision, the dealer deals last two community cards, face up on the board, in addition to the three already dealt.',
                'Straight after this the dealer opens his cards. Using two cards dealt face down and the community cards, the dealer composes the highest five card combination for the himself and for the player.',
                'The dealer qualifies with a pair of fours.',
                'If the dealer qualifies and his combination is lower than the players, then the bet is paid even money and the ante in accordance with a payout table.',
                'The player has a right to make a bet on the bonus. This is an independent bet and is made in accordance with the players wish. The bonus bet should be made at the same time with the ante, before the cards are dealt and qualifies in the first five cards with a combination of 2 pairs or higher. The bet does not depend on the size of the ante, but should not exceed the ante. All winning combinations are paid 7:1.',    
            ]
        ],
        [
            'title' => 'Six Card Poker',
            'sub_title' => 'Six Card Poker is a poker variant game played heads up against the dealer. The object of the game is to win by having a better five-card poker hand than the dealer using six cards. The game is played using one standard 52-card deck. The game uses standard poker rules for scoring and comparing hands. Players may not share information about their cards with other players.',
            'list' => [
                'To begin, the player makes an Ante Bet.',
                'The dealer deals six cards face down to the player. The dealer deals to himself, three cards face up and three cards face down.',
                'The player examines his cards and must either fold the hand, losing his Ante, or raise by making an additional wager that is equal to his Ante.',
                'The dealer reveals his hole cards and compares his best five-card hand to the player\'s best five-card hand. The dealer must qualify with ace-king or higher.',
                'If the dealer does not qualify, then the player\'s Ante bet is a push. The raise bet is resolved as follows:',
                'If the dealer\'s hand beats the player\'s hand, then the player loses his raise bet.',
                'If the player\'s hand beats the dealer\'s hand, then the player is paid 1 to 1 on his raise bet.',
                'In the event of a tie, the player\'s raise bet is a push.',
                'If the dealer does qualify:',
                'If the dealer\'s hand beats the player\'s hand, then the player loses his Ante and raise bets.',
                'If the player\'s hand beats the dealer\'s hand, then the player is paid 1 to 1 on his Ante and raise bets.',
                'In the event of a tie, the player\'s Ante and raise bets push.',
                
                'addition' => [
                    [
                        'title' => 'There are also two optional side bets, the Aces Up and Two-Way Bad Beat: ',
                        'list' => [
                            'Aces Up is a side bet based on the poker-value of the player\'s hand only.',
                            'The "Two-Way Bad Beat" pays if either the player or the dealer has at least a pair of aces and loses. The higher the losing hand, the more the bet pays. In other words, it pays based on the worse hand between the player and the dealer, as long as they don\'t tie.',
                        ],
                    ]
                ],
            ]
        ],
    ]
];