<?php

return [
    'block' => [
        [ 
            'title' => 'Our City',
            'description' => [
                'CAI groups first casino in Georgia, the Casino International Batumi in the Georgian coastal city of Batumi, opened its doors to the public for the first time on 1st of October 2015.',
                'Located in the stylish Hilton Batumi Hotel, Casino International Batumi enjoys one of the best locations in town, near the popular 6 May Park and Seaside Boulevard.',
                'The city is situated in a subtropical zone, rich in agricultural produce such as citrus fruit and tea. Tourism plays a key role in the region.',
            ],
        ],

        [
            'title' => 'Our Casino',
            'description' => [
                'The spacious casino with its elegant European style design features a choice of 13 live gaming tables and 105 latest generation gaming machines, including electronic Roulette.',
                'If you enjoy great entertainment in a stylish setting with highly trained, friendly staff, then Casino International Batumi with its unique Georgian welcome is definitely the place for you.',
                'Batumi is a vibrant seaside city on the Black Sea coast and capital of Adjara, an autonomous republic in southwest Georgia. Often referred to as the Pearl of the Black Sea, Batumi is an important port, commercial and tourism center.',
            ],
        ],

        [
            'title' => 'Our Hosts',
            'description' => [
                'The spacious casino with its elegant European style design features a choice of 13 live gaming tables and 105 latest generation gaming machines, including electronic Roulette.',
                'If you enjoy great entertainment in a stylish setting with highly trained, friendly staff, then Casino International Batumi with its unique Georgian welcome is definitely the place for you.',
                'Batumi is a vibrant seaside city on the Black Sea coast and capital of Adjara, an autonomous republic in southwest Georgia. Often referred to as the Pearl of the Black Sea, Batumi is an important port, commercial and tourism center.',
            ],
        ],

        [
            'title' => 'Our Team',
            'description' => [
                'The spacious casino with its elegant European style design features a choice of 13 live gaming tables and 105 latest generation gaming machines, including electronic Roulette.',
                'If you enjoy great entertainment in a stylish setting with highly trained, friendly staff, then Casino International Batumi with its unique Georgian welcome is definitely the place for you.',
                'Batumi is a vibrant seaside city on the Black Sea coast and capital of Adjara, an autonomous republic in southwest Georgia. Often referred to as the Pearl of the Black Sea, Batumi is an important port, commercial and tourism center.',
            ],
        ],

        [
            'title' => 'Our Group',
            'description' => [
                'With its roots dating back to the first gaming operations in Austria, Casino Austria AG is the parent company of the Casinos Austria Group and has held concessions to operate 12 casinos in Austria since 1968. Every year, Casinos Austria welcomes over 2.4 million guests and offers them the enjoyment of playing high-quality casino games in a stylish ambience. Responsibility towards each and every player is a guiding principle in Casinos Austria\'s corporate culture.',
                'Founded in 1977 to consolidate the Casinos Austria Groups international activities under one roof, Casinos Austria International (CAI) has since gone from strength to strength and is now a leading player in the global gaming industry. CAI remains at the top of a constantly evolving and dynamic gaming industry by offering a comprehensive portfolio of casino management and development services.',
                'Casinos Austria International is known throughout the gaming industry for their expertise in managing casinos. CAI specializes in all aspects of casino management and development. Their international renown and reputation for their know-how and expertise is unrivaled in the industry. The company offers 400 gaming tables and 4650 slot machines to casino players around the world.',
            ],
        ],

        [
            'title' => 'Our Partners',
            'description' => [
                'CISNET GmbH is one of the leading providers for casino system integration solutions for the European and Asian markets. Their unique products and services cover the whole range of requirements for all aspects of casino operations including solutions for surveillance, cashdesk, slot operations, table game, customer relations and IT support services.',
                'DR Gaming Technology (DRGT©) provides a unique gaming system answer. DRGT© is the ideal partner to improve a current machine park regardless of the machine number, type, or age.',
                'CISNET GmbH is one of the leading providers for casino system integration solutions for the European and Asian markets. Their unique products and services cover the whole range of requirements for all aspects of casino operations including solutions for surveillance, cashdesk, slot operations, table game, customer relations and IT support services.',
            ],
        ],
        [
            'title' => 'Our Company Info',
            'box' => [
                [
                    'title' => 'Opening Hours',
                    'list' => [
                        '24 Hours per day',
                        '7 days per week',
                        '365 days per year'
                    ],
                    'footer' => 'We never close, you are welcome to visit us at all times',
                ],
                [
                    'title' => 'Where to find us',
                    'list' => [
                        'Route description in 3 lines for balanced look',
                        'Another line',
                        'And another',
                    ],
                    'footer' => 'Some parting words',
                ],
            ]
            
        ]
    ],

    'send_a_message' => 'Send a message',
    'name' => 'Your Name',
    'email' => 'Your Email Address',
    'message' => 'Your Message',
    'send' => 'Send'
    

];