<?php

return [
    'slider' => [
        [
            'title' => 'Don\'t want to go out tonight?<br>Continue the game on your own terms!',
            'button' => 'Join our Online Casino',

        ]
    ],
    'title' => 'Welcome to Casino International Batumi',
    'sub_title' => 'The venue with the best entertainment in Georgia',

    'block' => [
        [
            [
                'title' => 'Exciting Games',
                'description' => '105 slot machines to choose from, the magic of the green baize, games starting from USD 0.1, dozens of mind-blowing jackpots, poker tournaments and table games...',
            ],
            [
                'title' => 'Fascinating Events',
                'description' => 'Enjoy our programme of events. Weekly dance shows and the hottest international and national talents wait for you.',
            ],
            [
                'title' => 'Relaxing Lounge and Bar',
                'description' => 'Take a break and enjoy our unrivaled bar. No matter if you prefer a tasty scotch, fruity cocktails or a light snack: Your wish is our command...',
            ]
        ],
        [
            [
                'title' => 'Free Parking',
                'description' => 'Our parking area is monitored by trained security operators 24/7',
            ],
            [
                'title' => 'Free Entry',
                'description' => 'We want you to have a great time while visiting us without having to think about entry fees',
            ],
            [
                'title' => 'Free WIFI',
                'description' => 'Stay on top of the latest news with our free and unlimited WIFI access',
            ]
        ]
    ]

];