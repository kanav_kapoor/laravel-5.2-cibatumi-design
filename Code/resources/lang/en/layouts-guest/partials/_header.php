<?php

    return [
        'casino'        => 'Casino',
        'home'          => 'Home',
        'about_us'      => 'About Us',
        'info'          => 'Info',
        'contact'       => 'Contact',
        'games'         => 'Games',
        'table_games'   => 'Table Games',
        'online_casino' => 'Online Casino',
    ];