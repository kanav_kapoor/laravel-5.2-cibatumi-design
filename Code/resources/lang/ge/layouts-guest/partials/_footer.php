<?php
    return [   
       'about_us'               => 'About Us',
       'terms_and_conditions'   => 'Terms and Conditions',
       'regulatory_information' => 'Regulatory Information',
       'privacy_policy'         => 'Privacy Policy',
       'responsible_gaming'     => 'Responsible Gaming',
       'support'                => 'Support',
       'conect_on'              => 'Connect on',
    ];