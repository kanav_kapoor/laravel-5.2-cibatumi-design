<!DOCTYPE html>
<html>
    <head>
        <title>@section('title'){{'Casino International Batumi'}}@show</title>
        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('layouts-guest/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('layouts-guest/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('layouts-guest/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('layouts-guest/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('layouts-guest/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('layouts-guest/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('layouts-guest/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('layouts-guest/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('layouts-guest/apple-icon-180x180.png') }}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('layouts-guest/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('layouts-guest/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('layouts-guest/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('layouts-guest/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('layouts-guest/manifest.json') }}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ asset('ms-icon-144x144.png') }}">
        <meta name="theme-color" content="#ffffff">

        <!--Import Google Icon Font-->
        {!! Html::style('//fonts.googleapis.com/icon?family=Material+Icons') !!}
        <!--Import materialize.css-->
        {!! Html::style('layouts-guest/css/materialize.min.css') !!}

        <!--Import additional Icons-->  
        {!! Html::style('layouts-guest/css/materialdesignicons.min.css') !!}

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        
        {!! Html::style('layouts-guest/css/app.css') !!}
    </head>
    <body class="grey lighten-3">
        <ul id="about-dropdown" class="dropdown-content white black-text">
            <li><a class="black-text" href="about">About Us</a></li>
            <li><a class="black-text" href="about#details">Info</a></li>
            <li class="divider"></li>
            <li><a class="black-text" href="about#contact">Contact</a></li>
        </ul>
        <ul id="games-dropdown" class="dropdown-content white black-text">
            <li><a class="black-text" href="games#table">Table Games</a></li>
            <!--        <li><a class="black-text" href="games#slots">Slot Machines</a></li>-->
        </ul>
        @include('layouts-guest.partials._header')

        <main> @yield('content') </main>
        @include('layouts-guest.partials._footer')
        {!! Html::script('//code.jquery.com/jquery-2.1.1.min.js') !!}
        {!! Html::script('layouts-guest/js/materialize.min.js') !!}
        {!! Html::script('layouts-guest/js/app.js') !!}

        @yield('js')
    </body>
</html>