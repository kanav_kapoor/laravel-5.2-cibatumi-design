<footer class="page-footer white">
    <div class="container">
        <div class="row center black-text">
            <div class="col l2 m6 s12"><a href="about" class="black-text text-lighten-3">@lang('layouts-guest/partials/_footer.about_us')</a></div>
            <div class="col l2 m6 s12"><a href="#" class="black-text text-lighten-3">@lang('layouts-guest/partials/_footer.terms_and_conditions')</a></div>
            <div class="col l2 m6 s12"><a href="#" class="black-text text-lighten-3">@lang('layouts-guest/partials/_footer.regulatory_information')</a></div>
            <div class="col l2 m6 s12"><a href="#" class="black-text text-lighten-3">@lang('layouts-guest/partials/_footer.privacy_policy')</a></div>
            <div class="col l2 m6 s12"><a href="#" class="black-text text-lighten-3">@lang('layouts-guest/partials/_footer.responsible_gaming')</a></div>
            <div class="col l2 m6 s12"><a href="#" class="black-text text-lighten-3">@lang('layouts-guest/partials/_footer.support')</a></div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row black-text">
                <div class="col l4 m12 s12">© 2016 Casino International Batumi</div>
                <div class="col l4 m12 s12 center-align"><a class="black-text text-lighten-4" href="https://www.casinosaustriainternational.com/" target="_blank"><img src="{{ asset('layouts-guest/cai.png') }}" style="padding-top:10px"/></a></div>
                <div class="col l4 m12 s12 right-align">@lang('layouts-guest/partials/_footer.conect_on') <i class="mdi mdi-facebook-box"></i> <i class="mdi mdi-google-plus-box"></i> <i class="mdi mdi-linkedin-box"></i></div>
            </div>
        </div>
    </div>
</footer>