<header>
    <div class="navbar-fixed">
        <nav class="white black-text">
            <div class="nav-wrapper">
                <a href="{{route('index')}}" class="brand-logo left hide-on-med-and-down"><img class="responsive-img" src="{{ asset('layouts-guest/cibatumi-hilton-logo-320x64-black.png') }}"/></a>
                <a href="{{route('index')}}" class="brand-logo left hide-on-small-only hide-on-large-only"><img class="responsive-img" style="padding-left:32px" src="{{ asset('layouts-guest/cibatumi-hilton-logo-320x64-black.png') }}"/></a>
                <a href="{{route('index')}}" data-activates="mobile-demo" class="button-collapse hide-on-small-only hide-on-large-only"><i class="material-icons black-text">menu</i></a>
                <a href="{{route('index')}}" data-activates="mobile-demo" class="button-collapse hide-on-med-and-up"><img class="responsive-img" src="{{ asset('layouts-guest/cai-menu-icon-56x56-alt-black.png') }}"/></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li>
                        <a class="about-dropdown-button black-text" href="about" data-activates="about-dropdown" data-beloworigin="true" data-hover="true" data-constrainwidth="true">
                        <i class="material-icons left">business</i>@lang('layouts-guest/partials/_header.casino')<i class="material-icons right">arrow_drop_down</i>
                        </a>
                    </li>
                    <li>
                        <a class="games-dropdown-button black-text" href="games" data-activates="games-dropdown" data-beloworigin="true" data-hover="true" data-constrainwidth="true">
                        <i class="mdi mdi-cards-playing-outline left"></i>@lang('layouts-guest/partials/_header.games')<i class="material-icons right">arrow_drop_down</i>
                        </a>
                    </li>
                    <li>
                        <a class="waves-effect waves-dark black-text" href="http://online.cibatumi.com">
                        <i class="material-icons left">tablet_android</i>@lang('layouts-guest/partials/_header.online_casino')
                        </a>
                    </li>
                </ul>
                <ul id="nav-mobile" class="right show-on-med-and-down hide-on-large-only">
                    <li>
                        <a class="waves-effect waves-dark black-text" href="http://online.cibatumi.com">
                        <i class="material-icons left">tablet_android</i>@lang('layouts-guest/partials/_header.online_casino')
                        </a>
                    </li>
                </ul>
                <ul class="side-nav" id="mobile-demo">
                    <li>
                        <a class="waves-effect waves-light" href="/">
                        <i class="material-icons left">home</i>@lang('layouts-guest/partials/_header.home')
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="waves-effect waves-light" href="about">
                        <i class="material-icons left">business</i>@lang('layouts-guest/partials/_header.casino')
                        </a>
                    </li>
                    <li>
                        <a class="waves-effect waves-light" href="about#details">
                        <i class="material-icons left">information_outline</i>@lang('layouts-guest/partials/_header.info')
                        </a>
                    </li>
                    <li>
                        <a class="waves-effect waves-light" href="about#contact">
                        <i class="mdi mdi-email-outline"></i>@lang('layouts-guest/partials/_header.contact')
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="waves-effect waves-light" href="games">
                        <i class="mdi mdi-cards-playing-outline left">person</i>@lang('layouts-guest/partials/_header.games')
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="waves-effect waves-dark black-text" href="https://online.cibatumi.com">
                        <i class="material-icons left">tablet_android</i>@lang('layouts-guest/partials/_header.online_casino')
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>