@extends('layouts-guest.app')

@section('content')
    <div class="parallax-container">
        <div class="parallax"><img src="{{ asset('layouts-guest/img/game-roulette.jpg') }}">
        </div>
    </div>
    <div class="section white">
        <div class="row container">
            <h2 class="header">@lang('games.block.0.title')</h2>
            <div class="col l12 m12 s12">
                <p>@lang('games.block.0.sub_title')</p>
                <li>@lang('games.block.0.list.0')</li>
                <li>@lang('games.block.0.list.1')</li>
                <li>@lang('games.block.0.list.2')</li>
                <li>@lang('games.block.0.list.3')</li>
                <li>@lang('games.block.0.list.4')</li>
                <li>@lang('games.block.0.list.5')</li>
                <li>@lang('games.block.0.list.6')</li>
                <li>@lang('games.block.0.list.7')</li>
            </div>
        </div>
    </div>
    <div class="parallax-container">
        <div class="parallax"><img src="{{ asset('layouts-guest/img/game-slots.jpg') }}"></div>
    </div>
    <div class="section white">
        <div class="row container">
            <h2 class="header">@lang('games.block.1.title')</h2>
            <div class="col l12 m12 s12">
                <p>@lang('games.block.1.sub_title')</p>
                <li>@lang('games.block.1.list.0')</li>
                <li>@lang('games.block.1.list.1')</li>
                <li>@lang('games.block.1.list.2')</li>
                <li>@lang('games.block.1.list.3')</li>
                <li>@lang('games.block.1.list.4')</li>
                <li>@lang('games.block.1.list.5')</li>
                <li>@lang('games.block.1.list.6')</li>
                <li>@lang('games.block.1.list.7')</li>
                <li>@lang('games.block.1.list.8')</li>
                <li>@lang('games.block.1.list.9')</li>
                <li>@lang('games.block.1.list.10')</li>
                <li>@lang('games.block.1.list.11')</li>
                
                <p>@lang('games.block.1.list.addition.0.title')
                <li>@lang('games.block.1.list.addition.0.list.0')</li>
                <li>@lang('games.block.1.list.addition.0.list.1')</li>
                </p>
            </div>
        </div>
    </div>
@stop