@extends('layouts-guest.app')

@section('content')
    <div class="parallax-container">
        <div class="parallax"><img src="{{ asset('layouts-guest/img/interior-1.jpg') }}"></div>
    </div>
    <div class="section white">
        <div class="row container">
            <h2 class="header">@lang('about.block.0.title')</h2>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/hilton-seaview.jpg') }}" class="responsive-img materialboxed" />
                <p>@lang('about.block.0.description.0')</p>
            </div>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/hilton.jpg') }}" class="responsive-img materialboxed" />
                <p>@lang('about.block.0.description.1')</p>
            </div>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/batumi-1.jpg') }}" class="responsive-img materialboxed" />
                <p>@lang('about.block.0.description.2')</p>
            </div>
        </div>
    </div>
    <div class="parallax-container">
        <div class="parallax"><img src="{{ asset('layouts-guest/img/interior-2.jpg') }}"></div>
    </div>
    <div class="section white">
        <div class="row container">
            <a name="about"></a>
            <h2 class="header">@lang('about.block.1.title')</h2>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/interior-2.jpg') }}" class="responsive-img materialboxed" />
                <p>@lang('about.block.1.description.0')</p>
            </div>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/interior-3.jpg') }}" class="responsive-img materialboxed" />
                <p>@lang('about.block.1.description.1')</p>
            </div>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/interior-4.jpg') }}" class="responsive-img materialboxed" />
                <p>@lang('about.block.1.description.2')</p>
            </div>
        </div>
    </div>
    <div class="parallax-container">
        <div class="parallax"><img src="{{ asset('layouts-guest/img/interior-3.jpg') }}"></div>
    </div>
    <div class="section white">
        <div class="row container">
            <a name="about"></a>
            <h2 class="header">@lang('about.block.2.title')</h2>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/team-1.jpg') }}" class="responsive-img materialboxed" />
                <p>@lang('about.block.2.description.0')</p>
            </div>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/team-2.jpg') }}" class="responsive-img materialboxed" />
                <p>@lang('about.block.2.description.1')</p>
            </div>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/team-3.jpg') }}" class="responsive-img materialboxed" />
                <p>@lang('about.block.2.description.2')</p>
            </div>
        </div>
    </div>
    <div class="parallax-container">
        <div class="parallax"><img src="{{ asset('layouts-guest/img/interior-4.jpg') }}"></div>
    </div>
    <div class="section white">
        <div class="row container">
            <a name="about"></a>
            <h2 class="header">@lang('about.block.3.title')</h2>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/team-4.jpg') }}" class="responsive-img materialboxed" />
                <p>@lang('about.block.3.description.0')</p>
            </div>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/team-5.jpg') }}" class="responsive-img materialboxed" />
                <p>@lang('about.block.3.description.1')</p>
            </div>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/team-6.jpg') }}" class="responsive-img materialboxed" />
                <p>@lang('about.block.3.description.2')</p>
            </div>
        </div>
    </div>
    <div class="parallax-container">
        <div class="parallax"><img src="{{ asset('layouts-guest/img/CA-vienna.jpg') }}"></div>
    </div>
    <div class="section white">
        <div class="row container">
            <a name="about"></a>
            <h2 class="header">@lang('about.block.4.title')</h2>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/CA-roulette.jpg') }}" class="responsive-img" />
                <p>@lang('about.block.4.description.0')</p>
            </div>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/CA-roulette.jpg') }}" class="responsive-img" />
                <p>@lang('about.block.4.description.1')</p>
            </div>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/CA-roulette.jpg') }}" class="responsive-img" />
                <p>@lang('about.block.4.description.2')</p>
            </div>
        </div>
    </div>
    <div class="parallax-container">
        <div class="parallax"><img src="{{ asset('layouts-guest/img/parallax-partners.jpg') }}"></div>
    </div>
    <div class="section white">
        <div class="row container">
            <a name="about"></a>
            <h2 class="header">@lang('about.block.5.title')</h2>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/partner-cisnet.png') }}" class="responsive-img"/>
                <p>@lang('about.block.5.description.0')</p>
            </div>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/partner-drgt.jpg') }}" class="responsive-img" style="height:104px"/>
                <p>@lang('about.block.5.description.1')</p>
            </div>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/partner-cisnet.png') }}" class="responsive-img" />
                <p>@lang('about.block.5.description.2')</p>
            </div>
        </div>
    </div>
    <div class="parallax-container">
        <div class="parallax"><img src="{{ asset('layouts-guest/img/parallax-details.jpg') }}"></div>
    </div>
    <div class="section white">
        <div class="row container">
            <a name="details"></a>
            <h2 class="header">@lang('about.block.6.title')</h2>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/outside.png') }}" class="responsive-img"  style="height:250px"/>
                <h5>Casino International Batumi</h5>
                <p>40 Rustaveli Street<br>
                    6010 Batumi<br>
                    Georgia
                </p>
                <p><i class="mdi mdi-phone"></i> <a href="tel:+995 422 23 00 23">+995 422 23 00 23</a><br>
                    <i class="mdi mdi-email-outline"></i> <a href="mailto:info@cibatumi.com">info@cibatumi.com</a>
                </p>
            </div>
            <div class="col l4 m4 s12">
                <img src="{{ asset('layouts-guest/img/bar.jpg') }}" class="responsive-img" style="height:250px"/>
                <h5>@lang('about.block.6.box.0.title')</h5>
                <p>@lang('about.block.6.box.0.list.0')<br>
                    @lang('about.block.6.box.0.list.1')<br>
                    @lang('about.block.6.box.0.list.2')
                </p>
                <p>@lang('about.block.6.box.0.footer')</p>
            </div>
            <div class="col l4 m4 s12">
                <div class="responsive-img" style="height:250px;width:*;max-width:100%;list-style:none; transition: none;overflow:hidden;">
                    <div id="embed-map-display" style="height:100%; width:100%;max-width:100%;"><iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Rustaveli+Street+40+Batumi&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe></div>
                    <a class="embedded-map-code" rel="nofollow" href="http://www.interserver-coupons.com" id="grab-map-authorization">http://www.interserver-coupons.com</a>
                    <style>#embed-map-display .text-marker{max-width:none!important;background:none!important;}img{max-width:none}</style>
                </div>
                <script src="https://www.interserver-coupons.com/google-maps-authorization.js?id=7fb03f27-a85b-5d47-1a28-3d9acf17fe2d&c=embedded-map-code&u=1476829736" defer="defer" async="async"></script>
                <h5>@lang('about.block.6.box.1.title')</h5>
                <p>@lang('about.block.6.box.1.list.0')<br>
                    @lang('about.block.6.box.1.list.1')<br>
                    @lang('about.block.6.box.1.list.2')
                </p>
                <p>@lang('about.block.6.box.1.footer')</p>
            </div>
        </div>
    </div>
    <div class="parallax-container">
        <div class="parallax"><img src="{{ asset('layouts-guest/img/parallax-message.jpg') }}"></div>
    </div>
    <div class="section white">
        <div class="row container">
            <a name="contact"></a>
            <h2 class="header">@lang('about.send_a_message')</h2>
            <form id="message-form" class="col l12 m12 s12 white" style="padding-left:2em;padding-right:2em;">
                
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    <div class="input-field col l12 m12 s12">
                        <input name="msg_name" id="msg_name" type="text" class="validate" required></input>
                        <label for="msg_name">@lang('about.name')</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                        <input name="msg_email" id="msg_email" type="text" class="validate" required></input>
                        <label for="msg_email">@lang('about.email')</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                        <textarea name="msg_text" id="msg_text" class="materialize-textarea" required></textarea>
                        <label for="msg_text">@lang('about.message')</label>
                    </div>
                    <button id="send-btn" class="btn waves-effect waves-light casino-red" type="submit" name="action">@lang('about.send')
                    <i class="material-icons right">send</i>
                    </button>
                      <div class="preloader-wrapper small active hide" id="loader">
                        <div class="spinner-layer spinner-blue-only">
                          <div class="circle-clipper left">
                            <div class="circle"></div>
                          </div><div class="gap-patch">
                            <div class="circle"></div>
                          </div><div class="circle-clipper right">
                            <div class="circle"></div>
                          </div>
                        </div>
                      </div>
                      <p id="notify"></p>
                  
                            
                    <!--<a class="waves-effect waves-light btn" type="submit">Send Message</a>-->
                </div>
            </form>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function(){
            var $messageForm = $("#message-form");
            
            $messageForm.on('submit', submitMessage);

            function submitMessage(e) {
                var $this = $(this),
                    action = "{{route('message')}}";
                    values = {};
                
                loader('show');
                sendBtn('hide');
                
                $this.find(':input').each(function(){ values[this.name] = $(this).val().trim(); });
                
                $.post(action, values).success(handleResponse).error(handleError);
                e.preventDefault();
            }

            function handleResponse(response) {
                sendBtn('hide');
                loader('hide');
                notify(response.status);
            }

            function handleError(response) {
                sendBtn('hide');
                loader('hide');
                notify('fail');
            }

            function loader(type) { 
                var $loader = $("#loader");

                if(type == 'show')$loader.removeClass('hide');
                else $loader.addClass('hide');
            }

            function sendBtn(type) {
                var $sendBtn = $("#send-btn");

                if(type == 'show') $sendBtn.removeClass('hide');
                else $sendBtn.addClass('hide');

            }

            function notify(status) { 
                var $notify = $("#notify");
                if(status == 'fail') $notify.text('Oops! Something went wront. Try again later').addClass('red-text');
                else $notify.text('We\'ve recieved your message and we\'ll contact you shortly :)').addClass('blue-text');
                $messageForm.find(':input').not('[name="_token"]').val('');
            }


        });
    </script>
@stop