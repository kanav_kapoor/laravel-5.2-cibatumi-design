@extends('layouts-guest.app')

@section('content')
    <!-- Row 1 -->
    <!-- Carousel / Slider - Full width -->
    <div class="carousel carousel-slider center hide-on-small-only" data-indicators="true">
        <div class="carousel-item black-text carousel-item-background" href="#three!" style="background-image:url({{ asset('layouts-guest/slider.jpg')}})">
            <h2 class="carousel-item-header"><br>@lang('index.slider.0.title')<br></h2>
            <!--                        <h2 class="carousel-item-text">Continue the game at your own pace!</h2>-->
            <a href="https://online.cibatumi.com" class="casino-red waves-effect waves-light btn-large"><i class="material-icons left">tablet_android</i>@lang('index.slider.0.button')</a>
        </div>
    </div>
    <!-- Row 2 -->
    <div class="row container" style="margin-top:40px">
        <div class="col l12 m12 s12 center"><span style="font-weight:bold;font-size:3em">@lang('index.title')</span><br><span style="font-size:2em">@lang('index.sub_title')</span></div>
    </div>
    <div class="row container" style="margin-top:20px">
        <div class="col l4 m4 s12">
            <div class="icon-block">
                <h2 class="center casino-red-text"><i class="mdi mdi-cards-playing-outline"></i></h2>
                <h5 class="center">@lang('index.block.0.0.title')</h5>
                <p class="light">@lang('index.block.0.0.description')</p>
            </div>
        </div>
        <div class="col l4 m4 s12">
            <div class="icon-block">
                <h2 class="center casino-red-text"><i class="mdi mdi-piano"></i></h2>
                <h5 class="center">@lang('index.block.0.1.title')</h5>
                <p class="light">@lang('index.block.0.1.description')</p>
            </div>
        </div>
        <div class="col l4 m4 s12">
            <div class="icon-block">
                <h2 class="center casino-red-text"><i class="mdi mdi-martini"></i></h2>
                <h5 class="center">@lang('index.block.0.2.title')</h5>
                <p class="light">@lang('index.block.0.2.description')</p>
            </div>
        </div>
    </div>
    <div class="parallax-container">
        <div class="parallax"><img src="{{ asset('layouts-guest/img/outside.png') }}"></div>
    </div>
    <div class="section white">
        <div class="row container">
            <div class="col l4 m4 s12">
                <div class="icon-block">
                    <h2 class="center casino-red-text"><i class="mdi mdi-car"></i></h2>
                    <h5 class="center">@lang('index.block.0.0.title')</h5>
                <p class="light">@lang('index.block.0.0.description')</p>
                </div>
            </div>
            <div class="col l4 m4 s12">
                <div class="icon-block">
                    <!--<h2 class="center casino-red-text"><i class="mdi mdi-weather-night"></i></h2>-->
                    <h2 class="center casino-red-text"><i class="mdi mdi-walk"></i></h2>
                    <h5 class="center">@lang('index.block.1.1.title')</h5>
                <p class="light">@lang('index.block.1.1.description')</p>
                </div>
            </div>
            <div class="col l4 m4 s12">
                <div class="icon-block">
                    <h2 class="center casino-red-text"><i class="mdi mdi-wifi"></i></h2>
                    <h5 class="center">@lang('index.block.1.2.title')</h5>
                <p class="light">@lang('index.block.1.2.description')</p>
                </div>
            </div>
        </div>
    </div>
@stop